<?php

// todo: remove and use only nginx/apache serving
/*Route::get('/{path?}', function () {
    return view('index');
});*/

Route::get('/routes', function (\Illuminate\Http\Request $request) {
    $routes =  collect(Route::getRoutes()->getRoutesByName());
    return $routes->only($request->key);
});
