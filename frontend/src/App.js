import React, {Component} from 'react';
import {Link, Route} from "react-router-dom";
import About from "./About";
import Topics from "./Topics";
import Routes from "./Routes";


class App extends Component {

    state = {
        userLogged: false
    };

    render() {
        return (
            <div>
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/about">About</Link>
                    </li>
                    <li>
                        <Link to="/topics">Topics</Link>
                    </li>
                    <li>
                        <Link to="/routes">Routes</Link>
                    </li>
                </ul>


                <hr/>

                <Route exact path="/about" component={About}/>
                <Route exact path="/topics" component={Topics}/>
                <Route exact path="/routes" component={Routes}/>
            </div>
        );
    }
}

export default App;
