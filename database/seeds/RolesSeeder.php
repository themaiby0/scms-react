<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Role::query()->insert([
            [
                'id' => 1,
                'title' => 'role.user',
            ], [
                'id' => 2,
                'title' => 'role.supervisor',
            ], [
                'id' => 3,
                'title' => 'role.moderator',
            ], [
                'id' => 4,
                'title' => 'role.admin',
            ], [
                'id' => 5,
                'title' => 'role.superadmin',
            ]
        ]);
    }
}
