<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::query()->create([
            'first_name' => 'Example',
            'last_name' => 'Account',
            'email' => 'admin@example.com',
            'password' => bcrypt('agrutne'),
            'role_id' => \App\Models\Role::ADMIN,
        ]);
    }
}
